#!/usr/bin/perl

use strict;
use warnings;
use diagnostics;

use Test::More 'no_plan';

use FindBin qw( $Bin );
use lib "$Bin/../noten";

use noten;

use_ok( 'noten' );
warn __PACKAGE__;
can_ok( __PACKAGE__, 'noten::zensieren' );
