#!/usr/bin/perl
use strict;
use warnings;
use diagnostics;

use Data::Dump qw( dump );
use FindBin qw( $Bin );
use lib "$Bin/noten";

use noten;

my $test = noten->new( aufgaben => 13 );

$test->aufgaben;

print $test->aufgaben . "\n";

print $test->note . "\n";

$test->note( 15 );
print $test->note . "\n";

$test->zensieren;

my $count = 1;

print "\n";
print "*" x 80;
print "\n\n";

sleep 1;

for my $key ( sort { $a <=> $b } keys %{$test->noten} ) {
    printf "Aufgabe %2d: %2d Punkte\n", $key, $test->noten->{$key};
}
