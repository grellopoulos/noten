package noten;

use strict;
use warnings;
use diagnostics;

use Moose;

has 'aufgaben', is => 'ro', isa => 'Num';

has 'note', is => 'rw', isa => 'Num', default => 0;

has 'noten' => (
    traits => ['Hash'],
    is => 'rw',
    isa => 'HashRef[Num]',
    default => sub { {} },
    handles => {
	set_option	 => 'set',
	get_option	 => 'get',
	has_no_options	 => 'is_empty',
	num_options	 => 'count',
	delete_option	 => 'delete',
	option_pairs	 => 'kv',
    },
);

sub zensieren {
    my $self = shift;

    for ( 1 .. $self->aufgaben - 1 ) {
	printf "Aufgabe %2d: ", $_;
	chomp( my $val = <STDIN> );
	$self->noten->{$_} = $val;
    }
}

1;
